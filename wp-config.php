<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'minimums');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Ty%%Oc3z`|^PV^c-~i}A->B}H-#r[!j]w|v,V+^7V*$o1OYX`qUX!2`Dy!>S^UFZ');
define('SECURE_AUTH_KEY',  'a0+*#yVtA->Q5HqI}QuV@3B.CQ|AB|-I7ZR(WfcrG2s?~Ep71xjj>21fTv%*OAf!');
define('LOGGED_IN_KEY',    '+g}Qw[wD~LnQV~t+sD+aHY-.Ar[]nA<%KFrBnfQ8}t$chV;U9JB6<!os>UucR[=~');
define('NONCE_KEY',        'k=?$#@px|~1-CiE&e>a8zToW-68:dwso=+Jo:_-_`eaXWe.FmuqPj$g>0NMaE@i{');
define('AUTH_SALT',        'J4Z77K Y6dUYIY)<X|U33&i`-h`@MT/Z>rbOiwpIB+9^5ILlV PX||7sq;XqaF(^');
define('SECURE_AUTH_SALT', 'jeNy,g-5V!f8@+MpJIeIYj8l!{+B;_+?|ik2i$m[xOsJ$R2KMnQ.fq*?N2X:}~0+');
define('LOGGED_IN_SALT',   'e/1i,v~e0^nb[FifR7JUB]+m~1cU7w}|+n]u v2)(elI@o=Pfs-.GE)|sy[BOymO');
define('NONCE_SALT',       ' j*lK$Ge{efP|Qhg{IS5}+k~1:E$F8-Fml- ER.Fr8X7,le<k$X/5SpS@!ZHb|p&');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
