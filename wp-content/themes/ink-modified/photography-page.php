<?php /* Template Name: Photography Page */ ?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, minimal-ui">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link type="text/css" rel="stylesheet" href="http://fast.fonts.net/cssapi/0b09af0a-7a35-4e17-b025-0b5bc2733af4.css"/>

<?php wp_head(); ?>
</head>
<body>


      <div class="page-branding">
        <?php if ( stag_get_logo()->has_logo() ) : ?>
          <a class="custom-logo" title="<?php esc_attr_e( 'Home', 'stag' ); ?>" href="<?php echo esc_url( home_url( '/' ) ); ?>"></a>
        <?php else : ?>
          <h1 class="site-title"><a href="<?php echo esc_url( home_url() ); ?>"><!-- <?php bloginfo( 'name' ); ?>--> m.</a></h1>
        <?php endif; ?>
      </div>




<header class="photography-header">
  <div class="photography-header-text">
    <h1>Be a Photographer for Minimums</h1>
    <p>Minimums features the most interesting pocessions of the world's most interesting people. By definition, those people are located all over the world, and as such, we look for great photographers everywhere.</p>
    <p>Help us tell the next Minimums story.</p>
    <a href="mailto:hello@needwant.com" class="get-in-touch-button">Get In Touch</a>
  </div>
</header>

<div class="main">
  <div class="photography-main width">
    <h2>Why Shoot for us?</h2>

    <div class="photography-row">
      <div class="photography-row-text">
        <h3>Exposure &amp; Well Placed Credit</h3>
        <p>We proudly highlight and credit each photographer in a well placed located in each feature. We'll also link to your website.</p>
      </div>

      <div class="photography-row-image">
        <img src="/wp-content/themes/ink-modified/exposure.png" "photographer exposure" />
      </div>
    </div>

    <div class="photography-row">
      <div class="photography-row-text">
        <h3>Artistic Freedom</h3>
        <p>With the exception of a few minor constraints, we trust our photographers to set the best style and tone of the shoot.</p>
      </div>

      <div class="photography-row-image">
        <img src="/wp-content/themes/ink-modified/freedom.jpg" "photographer artistic freedom" />
      </div>
    </div>

    <div class="photography-row">
      <div class="photography-row-text">
        <h3>On Going Social Media Shoutouts</h3>
        <p>In addition to the shoutout on minimums.com, we'll also credit your work in our social media accounts – a great way for you to gain more followers &amp; fans</p>
      </div>

      <div class="photography-row-image">
        <img src="/wp-content/themes/ink-modified/social.jpg" "photographer exposure" />
      </div>
    </div>

    <a href="mailto:hello@needwant.com" class="shoot-the-next-button">Shoot The Next Minimums Feature</a>

  </div>
</div>

<?php get_footer(); ?>

