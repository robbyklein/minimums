<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package Stag_Customizer
 * @subpackage Ink
 */
?>

<footer class="master-footer">
	<div class="width">
		<div class="footer-box">
			<h3>Navigate</h3>
			<ul>
				<li><a href="mailto:hello@needwant.com">Contact</a></li>
				<li><a href="/be-a-minimums-photographer">Be a Photographer</a></li>
				<li><a href="mailto:hello@needwant.com">Advertise</a></li>
			</ul>
		</div>

		<div class="footer-box">
			<h3>Social</h3>
			<ul>
				<li><a href="https://www.facebook.com/minimumshq">Facebook</a></li>
				<li><a href="https://twitter.com/minimumshq">Twitter</a></li>
				<li><a href="https://instagram.com/minimumshq/">Instagram</a></li>
			</ul>
		</div>

		<div class="foot-copy">
			<span>
				<a href="http://needwant.com"><svg class="needwant-logo" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 157 69"><path d="M136.945,0.007 C136.945,0.007 136.945,35.293 136.945,35.293 C136.945,35.293 136.452,35.293 136.452,35.293 C136.452,35.293 118.471,5.034 118.471,5.034 C116.201,1.289 113.927,0.007 109.383,0.007 C109.383,0.007 91.305,0.007 91.305,0.007 C91.305,0.007 91.305,35.293 91.305,35.293 C91.305,35.293 90.859,35.293 90.859,35.293 C90.859,35.293 72.879,5.034 72.879,5.034 C70.607,1.289 68.335,0.007 63.790,0.007 C63.790,0.007 61.662,0.007 61.662,0.007 C61.662,0.007 61.662,0.000 61.662,0.000 C61.662,0.000 45.640,0.000 45.640,0.000 C45.640,0.000 45.640,35.284 45.640,35.284 C45.640,35.284 45.147,35.284 45.147,35.284 C45.147,35.284 27.167,5.027 27.167,5.027 C24.895,1.281 22.623,0.000 18.078,0.000 C18.078,0.000 0.000,0.000 0.000,0.000 C0.000,0.000 0.000,68.991 0.000,68.991 C0.000,68.991 20.054,68.991 20.054,68.991 C20.054,68.991 20.054,29.567 20.054,29.567 C20.054,29.567 20.548,29.567 20.548,29.567 C20.548,29.567 41.887,63.966 41.887,63.966 C43.097,65.878 44.262,67.142 45.712,67.926 C45.712,67.926 45.712,67.934 45.712,67.934 C45.712,67.934 45.738,67.939 45.738,67.939 C47.121,68.680 48.765,68.991 50.975,68.991 C50.975,68.991 51.801,68.991 51.801,68.991 C51.801,68.991 51.846,69.000 51.846,69.000 C51.846,69.000 65.766,69.000 65.766,69.000 C65.766,69.000 65.766,29.576 65.766,29.576 C65.766,29.576 66.260,29.576 66.260,29.576 C66.260,29.576 87.598,63.973 87.598,63.973 C88.777,65.835 89.909,67.086 91.305,67.876 C91.305,67.876 91.305,67.877 91.305,67.877 C91.305,67.877 91.306,67.877 91.306,67.877 C92.719,68.674 94.403,69.000 96.686,69.000 C96.686,69.000 111.360,69.000 111.360,69.000 C111.360,69.000 111.360,29.576 111.360,29.576 C111.360,29.576 111.853,29.576 111.853,29.576 C111.853,29.576 133.192,63.973 133.192,63.973 C135.563,67.718 137.736,69.000 142.280,69.000 C142.280,69.000 157.000,69.000 157.000,69.000 C157.000,69.000 157.000,0.007 157.000,0.007 C157.000,0.007 136.945,0.007 136.945,0.007 Z"/></svg></a>
			</span>
			<p><a href="http://needwant.com">a need/want publication</a><br>
			All rights reserved. &copy; 2015.</p>
		</div>
	</div>
</footer>

<?php wp_footer(); ?>
<script type="text/javascript">
  var _gauges = _gauges || [];
  (function() {
    var t   = document.createElement('script');
    t.type  = 'text/javascript';
    t.async = true;
    t.id    = 'gauges-tracker';
    t.setAttribute('data-site-id', '5571cb2a92c6ac59e10017a4');
    t.setAttribute('data-track-path', 'https://track.gaug.es/track.gif');
    t.src = 'https://track.gaug.es/track.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(t, s);
  })();
</script>
</body>
</html>
