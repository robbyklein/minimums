(function() {
    tinymce.create('tinymce.plugins.YourYouTube', {
        init : function(ed, url) {
            ed.addButton('youryoutube', {
                title : 'Check it out button',
                image : url+'/youtube.png',
                onclick : function() {
                    var text = prompt("Text", "Enter button text");
                    var price = prompt("Price", "Enter button price");
                    var url = prompt("URL", "Enter button url");
                    var alt = prompt("Search Words", "(1 space seperated)");

                    if (text != null && text != 'undefined') {
                        ed.execCommand('mceInsertContent', false, '[checkit text="' + text + '" url="' + url + '" " alt="' + alt + '" price="' + price + '"]');
                    }
                }
            });
        },
        createControl : function(n, cm) {
            return null;
        },
        getInfo : function() {
            return {
                longname : "YouTube Shortcode",
                author : 'Brett Terpstra',
                authorurl : 'http://brettterpstra.com/',
                infourl : 'http://brettterpstra.com/',
                version : "1.0"
            };
        }
    });
    tinymce.PluginManager.add('youryoutube', tinymce.plugins.YourYouTube);
})();


