<div class="newsletter cf">
  <div class="width">
    <div class="newsletter-left">
      <h3>Subscribe for exclusive updates</h3>
      <p>We feature the most interesting possessions of the world’s most interesting people. Subscribe and never miss one!</p>
    </div>
    <div class="newsletter-right">

      <form id="mc-form2" class="mailing-list cf">
        <input id="mc-email2" type="email" value="" name="EMAIL" placeholder="you@email.com">
        <button type="submit"><svg class="right-arrow" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 39 36"><path d="M37.263,20.387 C37.263,20.387 23.438,34.214 23.438,34.214 C22.689,34.964 21.706,35.339 20.724,35.339 C19.741,35.339 18.758,34.964 18.008,34.214 C16.509,32.714 16.509,30.282 18.008,28.781 C18.008,28.781 25.276,21.512 25.276,21.512 C25.276,21.512 3.791,21.512 3.791,21.512 C1.690,21.512 -0.012,19.792 -0.012,17.670 C-0.012,15.549 1.690,13.829 3.791,13.829 C3.791,13.829 25.277,13.829 25.277,13.829 C25.277,13.829 18.008,6.559 18.008,6.559 C16.509,5.059 16.509,2.627 18.008,1.127 C19.508,-0.373 21.939,-0.373 23.439,1.127 C23.439,1.127 37.263,14.954 37.263,14.954 C37.983,15.675 38.387,16.652 38.387,17.670 C38.387,18.689 37.983,19.666 37.263,20.387 Z" /></svg></button>
       <label for="mc-email2"></label>
      </form>
    </div>
  </div>
</div>