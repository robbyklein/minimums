<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <main id="main">.
 *
 * @package Stag_Customizer
 * @subpackage Ink
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1, minimal-ui">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link type="text/css" rel="stylesheet" href="http://fast.fonts.net/cssapi/0b09af0a-7a35-4e17-b025-0b5bc2733af4.css"/>

<?php wp_head(); ?>

<script>(function() {
  var _fbq = window._fbq || (window._fbq = []);
  if (!_fbq.loaded) {
    var fbds = document.createElement('script');
    fbds.async = true;
    fbds.src = '//connect.facebook.net/en_US/fbds.js';
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(fbds, s);
    _fbq.loaded = true;
  }
  _fbq.push(['addPixelId', '1550854728502798']);
})();
window._fbq = window._fbq || [];
window._fbq.push(['track', 'PixelInitialized', {}]);
</script>
<noscript><img height="1" width="1" alt="" style="display:none" src="https://www.facebook.com/tr?id=1550854728502798&amp;ev=PixelInitialized" /></noscript>

</head>
<body <?php body_class(); ?> data-layout="<?php echo esc_attr( stag_site_layout() ); ?>">


<div id="page" class="hfeed site">

	<div id="content" class="site-content">

		<header id="masthead" class="site-header">

			<div class="site-branding">
				<?php if ( stag_get_logo()->has_logo() ) : ?>
					<a class="custom-logo" title="<?php esc_attr_e( 'Home', 'stag' ); ?>" href="<?php echo esc_url( home_url( '/' ) ); ?>"></a>
				<?php else : ?>
					<h1 class="site-title"><a href="<?php echo esc_url( home_url() ); ?>"><!-- <?php bloginfo( 'name' ); ?>--> m.</a></h1>
				<?php endif; ?>

				<!-- <p class="site-description"><?php bloginfo( 'description' ); ?></p> -->
			</div>

			<?php if ( ! is_author() && ( is_archive() || is_search() ) ) : ?>
			<div class="archive-header">
				<h3 class="archive-header__title"><?php echo stag_archive_title(); ?></h3>
			</div>
			<?php endif; ?>

		</header><!-- #masthead -->
